Name:           libXp
Version:        1.0.4
Release:        1
Summary:        LibXp runtime library for X.Org
License:        MIT
URL:            http://www.x.org
Source0:        https://www.x.org/pub/individual/lib/%{name}-%{version}.tar.xz

Patch0:         add-proto-files.patch

BuildRequires:  xorg-x11-util-macros xorg-x11-proto-devel libX11-devel make
BuildRequires:  libXext-devel libXau-devel libtool automake autoconf gettext

%description
LibXp runtime library for X.Org.

%package devel
Summary:        Development package for %{name}
Requires:       libXau-devel pkgconfig
Requires:       %{name} = %{version}-%{release}
BuildRequires:  xorg-x11-proto-devel

%description devel
This pacakge contains some header files and library files for
the development of %{name}.

%prep
%autosetup -p1

%build
export CPPFLAGS="$CPPFLAGS -I$RPM_BUILD_ROOT%{_includedir}"
autoreconf -v --install

%configure --disable-static
make


%install
rm -rf %{buildroot}
%make_install

%delete_la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc AUTHORS COPYING ChangeLog
%{_libdir}/libXp.so.*
%exclude %{_mandir}

%files devel
%{_includedir}/X11/extensions/Print.h
%{_includedir}/X11/extensions/Printstr.h
%{_libdir}/pkgconfig/printproto.pc
%{_libdir}/libXp.so
%{_libdir}/pkgconfig/xp.pc

%changelog
* Thu Feb 16 2023 lilong <lilong@kylinos.cn> - 1.0.4-1
- Upgrade to 1.0.4

* Wed Mar 11 2020 yangjian <yangjian79@huawei.com> - 1.0.3-6
- Fix dependencies

* Fri Feb 28 2020 yanglijin <yanglijin@huawei.com> - 1.0.3-5
- provides pkgconfig(printproto)

* Sun Jan 19 2020 yanglijin <yanglijin@huawei.com> - 1.0.3-4
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: modify buildrequires

* Wed Dec 11 2019 catastrowings <jianghuhao1994@163.com> - 1.0.3-3
- openEuler init
